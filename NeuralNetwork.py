# -*- coding: utf-8 -*-

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Apr 28 13:09:16 2018

@author: petervandoorn
"""
#Artificial Neural Networks
 
#run in terminal the next 3 commands to setup the libraries

#Installing THean
# pip install --upgrade --no-deps git+git://github.com/Theano/Theano.git
# Will enable to run software on GPU
 
# Installing Tensorflow
# Install Tensorflow from the website: https://www.tensorflow.org/versions/r0.12/get_started/os_setup.html

#Installing Keras
 #pip install --upgrade keras 
 
# Importing the libraries
import pandas as pd

# Importing the dataset
dataset = pd.read_csv('Churn_Modelling.csv')
X = dataset.iloc[:, 3:13].values
y = dataset.iloc[:, 13].values
 
## 1.0 Data preprocessing

# Encoding categorical data
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
labelencoder_X_1 = LabelEncoder()
X[:, 1] = labelencoder_X_1.fit_transform(X[:, 1])
labelencoder_X_2 = LabelEncoder()
X[:, 2] = labelencoder_X_2.fit_transform(X[:, 2])
onehotencoder = OneHotEncoder(categorical_features = [1])
X = onehotencoder.fit_transform(X).toarray()
X = X[:, 1:]

# Splitting the dataset into the Training set and Test set
from sklearn.cross_validation import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 0)

# Feature Scaling
#very needed to apply feature scaling to ease the very complex calculations that are to be made
from sklearn.preprocessing import StandardScaler
sc = StandardScaler()
X_train = sc.fit_transform(X_train)
X_test = sc.transform(X_test) 
 
# 2.0 Let's make the Artifician Neural Network

import keras
from keras.models import Sequential
from keras.layers import Dense
 
# 3.0 Initialize the Neural Network
#we just call our network Classifier since we will use it later in the predictions
classifier = Sequential()

#3.1 Adding input layer and first hidden layer
classifier.add(Dense(output_dim = 6, init = 'uniform', activation = 'relu', input_dim = 11))

#3.2 adding the second and third hidden layer
classifier.add(Dense(output_dim = 6, init = 'uniform', activation = 'relu'))
classifier.add(Dense(output_dim = 6, init = 'uniform', activation = 'relu'))

#3.3 adding the output layer
#we choose a sigmoid function instead of rectifier (relu), since it will give a probability instead of a binary output
#This is usefull since we want a probability for our output
#if we have a output with more than 1 nodes use softmax instead of sigmoid. That will enable the sigmoid function bu t to mutltiple outpurs. 
classifier.add(Dense(output_dim = 1, init = 'uniform', activation = 'sigmoid'))

#4.0 Compile neural network
#We apply stachastic gradient descent to the NN
classifier.compile(optimizer = 'adam', loss = 'binary_crossentropy', metrics = ['accuracy'])

## 5.0 Fitting the ANN to the Training set
classifier.fit(X_train, y_train, batch_size = 10, nb_epoch = 10)

# 6. predicting the Test set results
y_pred = classifier.predict(X_test)
#setting a treshold, for a binary output. We set it at 50% now, so if the probability is more than 50% then true is returned. 
#A treshold should be set according to the problem we are solving. For example in medical situations you want a lower treshold. 
y_pred = (y_pred > 0.5)


#6.1 Confusion matrix
from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y_test, y_pred)

#7.0 calculating and printing the Performance
import time
Accuracy = (cm[0,0] + cm [1,1])/(cm[0,0] + cm [1,1] + cm[1,0] + cm [0,1])
Precision = (cm[0,0])/(cm[0,0]+cm[1,1])
Recall = (cm[0,0])/(cm[0,0]+cm[0,1])
F1Score = (2*Precision*Recall)/(Precision + Recall)

print("Finishing calculations")
time.sleep(2)
print(".")
time.sleep(1)
print(".")
time.sleep(1)
print(".")
time.sleep(1)
print(".")
time.sleep(1)
print("Cooling down the robots")
time.sleep(2)
print(".")
time.sleep(1)
print(".")
time.sleep(1)
print(".")
time.sleep(1)
print(".")
time.sleep(1)
print("Turning down the power")
time.sleep(2)
print(".")
time.sleep(1)
print(".")
time.sleep(1)
print(".")
time.sleep(1)
print(".")
time.sleep(1)
print("the results are in")
print(".")
time.sleep(1)
print(".")
time.sleep(1)
print(".")
time.sleep(1)
print(".")
time.sleep(1)

print("3.")
time.sleep(1)
print("2.")
time.sleep(1)
print("1.")
time.sleep(1)

print("The results:")
time.sleep(1)
print("The F1 score                                 =", F1Score*100,"%")
print("The Accuracy                                 =", Accuracy*100,"%")
print("Precision                                    =", Precision*100,"%")
print("The Recall                                   =", Recall*100,"%")
print("Cyborgs that escaped to kill the human race  = 3.8264313987 %")
